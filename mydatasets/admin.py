# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
import models

# Register your models here.
admin.site.register(models.Dataset)
admin.site.register(models.Version)
admin.site.register(models.DatasetReview)
admin.site.register(models.Development)
admin.site.register(models.Gender)
admin.site.register(models.Neuron)
admin.site.register(models.Species)
admin.site.register(models.BrainRegion1)
admin.site.register(models.BrainRegion2)
admin.site.register(models.BrainRegion3)
admin.site.register(models.CellType1)
admin.site.register(models.CellType2)
admin.site.register(models.CellType3)
admin.site.register(models.StainMethod)
admin.site.register(models.SlicingDirection)
admin.site.register(models.Strain)
admin.site.register(models.ReconstructionSoftware)
admin.site.register(models.ObjectiveType)
admin.site.register(models.ProtocolDesign)
admin.site.register(models.ExperimentCondition)
admin.site.register(models.OriginalFormat)
