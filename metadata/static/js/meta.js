function ConfirmDelete() {
  var x = confirm("Are you sure you want to delete?");
  if (x)
      return true;
  else
    return false;
}

function ConfirmArchive() {
  var x = confirm("Are you sure you want to change Archive status of this dataset?");
  if (x)
      return true;
  else
    return false;
}

// enable and disable selections
function enableDisable(bEnable, textBoxID) {
    document.getElementById(textBoxID).disabled = !bEnable
}

// generic select change detection
$(document).ready(function(){
    $('select').change(function(){
        var selected_value = $(this).val();
        var selected_name = $(this).attr("name");
        var new_selected_name = "new_" + selected_name;
        var changed_to = $(this).find('option:selected').text();
        if(selected_value == "" || selected_value == "Not in the list?"){
            $("input[name=" + new_selected_name + "]").prop('disabled', false);
            $("input[name=" + new_selected_name + "]").show(1000);
            $("input[name=" + new_selected_name + "]").val("");
            // alert('if is executed!');
        } else {
            $("input[name=" + new_selected_name + "]").val("");
            $("input[name=" + new_selected_name + "]").hide(1000);
        }
    });
});

// generic multi select change detection
$(document).ready(function(){
    $('#additional-brain-region, #additional-cell-type').change(function(){
        var selected_value = $(this).is(':checked');;
        var selected_name = $(this).attr("name");
        if(selected_name == "new-region3-input"){
            var new_selected_name = "new_brain_region3";
        } else if(selected_name=="new-cell3-input"){
            var new_selected_name = "new_cell_type3";
        }
        // var changed_to = $(this).find('option:selected').text();
        // console.log(selected_name + " is changed to " + selected_value);
        if(selected_value){
            // $("textarea[name=" + new_selected_name + "]").prop('disabled', false);
            $("textarea[name=" + new_selected_name + "]").show(1000);
        } else if(selected_value==false){
            $("textarea[name=" + new_selected_name + "]").val("");
            $("textarea[name=" + new_selected_name + "]").hide(1000);
        }
    });
});

// select change detection for pixel
$(document).ready(function(){
    $('#id_numerical_units').change(function(){
        var selected_value = $(this).val();
        var selected_name = $(this).attr("name");
        var new_selected_name = "pixel_size";
        // alert(selected_name + " is changed to " + selected_value);
        // console.log(selected_name + " is changed to " + selected_value);
        var changed_to = $(this).find('option:selected').text();
        if(selected_value == "pixel"){
            $("input[name=" + new_selected_name + "]").prop('disabled', false);
            $("input[name=" + new_selected_name + "]").show(1000);
            $("input[name=" + new_selected_name + "]").val("");
            // alert('if is executed!');
        } else {
            // alert('else is executed!');
            $("input[name=" + new_selected_name + "]").val("");
            $("input[name=" + new_selected_name + "]").hide(1000);
            // $("input[name=" + new_selected_name + "]").prop('disabled', true);
            // $("input[name=" + new_selected_name + "]").val(changed_to);
        }
    });
});

// secondary level
$(document).ready(function() {
        $("#id_species, #id_cell_type1, #id_cell_type2, #id_brain_region1, #id_brain_region2").change(function () {
            var url = $("#id_cell_type1").attr("data-url");
            var SelectValue = $(this).val();
            var SelectName = $(this).attr("name");
            var parent1 = "None";
            if(SelectValue == ''){
                console.log('returning');
                return;
            }
            if(SelectName == 'cell_type1'){
                var next_select = "#id_cell_type2";
            } else if(SelectName == 'cell_type2')  {
                var next_select = "#id_cell_type3";
                var parent1 = document.getElementById("id_cell_type1").value;
                // var parent1 = $("#id_cell_type1").val();
            } else if(SelectName == 'brain_region1')  {
                var next_select = "#id_brain_region2";
                // console.log('logging region1 change');
            } else if(SelectName == 'brain_region2')  {
                var next_select = "#id_brain_region3";
                // var next_select = "keep-order";
                var parent1 = document.getElementById("id_brain_region1").value;
                // var parent1 = $("#id_brain_region1").val();
            } else if(SelectName == 'species')  {
                var next_select = "#id_strain";
            }
            // alert(CellType);
            $.ajax({ // initialize an AJAX request
                url: url, 
                data: {
                    'SelectValue': SelectValue,
                    'SelectName': SelectName,
                    'parent1': parent1,
                },
                success: function (data) {   //
                $(next_select).html(data);  //
                
                // update next level
                switch (next_select) {
                    case "#id_brain_region2":
                        $("#id_brain_region2").trigger("chosen:updated"); break;
                    case "#id_brain_region3":
                        $("#id_brain_region3").trigger("chosen:updated"); break;
                    case "#id_cell_type2":
                        $("#id_cell_type2").trigger("chosen:updated"); break;
                    case "#id_cell_type3":
                        $("#id_cell_type3").trigger("chosen:updated"); break;
                    case "#id_strain":
                        $("#id_strain").trigger("chosen:updated"); break;
                }
            }
        });
    });
});

// integrity
$(document).ready(function() {
        $("#id_dendrites, #id_axon, #id_neurites, #id_processes").change(function () {
            var ChangedtValue = $(this).is(':checked'); 
            var ChangedName = $(this).attr("name");
            // console.log(ChangedName + ', ' + ChangedtValue);
            if(ChangedName == 'axon' && ChangedtValue == true){
                document.getElementById("id_axon_integrity").disabled=false;
                $("#id_neurites, #id_processes").prop('checked', false);
                document.getElementById("id_processes_integrity").disabled=true;
                document.getElementById("id_neurites_integrity").disabled=true;
                document.getElementById("id_processes_integrity").selectedIndex=0;
                document.getElementById("id_neurites_integrity").selectedIndex=0;
            } else if(ChangedName == 'axon' && ChangedtValue == false){
                document.getElementById("id_axon_integrity").disabled=true;
                document.getElementById("id_axon_integrity").selectedIndex=0;
            }
            if(ChangedName == 'dendrites' && ChangedtValue == true){
                document.getElementById("id_dendrites_integrity").disabled=false;
                $("#id_neurites, #id_processes").prop('checked', false)
                document.getElementById("id_processes_integrity").disabled=true;
                document.getElementById("id_neurites_integrity").disabled=true;
                document.getElementById("id_processes_integrity").selectedIndex=0;
                document.getElementById("id_neurites_integrity").selectedIndex=0;
            } else if(ChangedName == 'dendrites' && ChangedtValue == false){
                document.getElementById("id_dendrites_integrity").disabled=true;
                document.getElementById("id_dendrites_integrity").selectedIndex=0;
            }
            if(ChangedName == 'neurites' && ChangedtValue == true){
                document.getElementById("id_neurites_integrity").disabled=false;
                $("#id_dendrites, #id_axon").prop('checked', false);
                document.getElementById("id_dendrites_integrity").disabled=true;
                document.getElementById("id_axon_integrity").disabled=true;
                document.getElementById("id_dendrites_integrity").selectedIndex=0;
                document.getElementById("id_axon_integrity").selectedIndex=0;
            } else if(ChangedName == 'neurites' && ChangedtValue == false){
                document.getElementById("id_neurites_integrity").disabled=true;
                document.getElementById("id_neurites_integrity").selectedIndex=0;
            }
            if(ChangedName == 'processes' && ChangedtValue == true){
                document.getElementById("id_processes_integrity").disabled=false;
                $("#id_dendrites, #id_axon").prop('checked', false);
                document.getElementById("id_dendrites_integrity").disabled=true;
                document.getElementById("id_axon_integrity").disabled=true;
                document.getElementById("id_dendrites_integrity").selectedIndex=0;
                document.getElementById("id_axon_integrity").selectedIndex=0;
            } else if(ChangedName == 'processes' && ChangedtValue == false){
                document.getElementById("id_processes_integrity").disabled=true;
                document.getElementById("id_processes_integrity").selectedIndex=0;
            }
    });
});

// working on hovers
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

// data table
$(document).ready(function() {
    $("#datasets").DataTable({
    "pagingType": "full_numbers",
    "ordering":true,
    "order": [[5, "desc"]],
    "columnDefs":[{
        "targets":6,
        "sortable":false,
        "searchable":false,
    }],
    });
});

// edit button fade in/out
$(document).ready(function(){
    $(parent.window.document).scroll(function() {
        // console.log('scrolling!');
        if ($(this).scrollTop() > 80) {
            $('#down').fadeIn();
        } else {
            $('#down').fadeOut();
        }
    });
});


$(document).ready(function(){
    $(parent.window.document).scroll(function() {
        // console.log('scrolling!');
        var scrollHeight   = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) / scrollHeight < 0.035) {
            // when scroll to bottom of the page
            $('#top').fadeOut();
        }
        else {
            $('#top').fadeIn();
        }
    });
});

// chosen
$(document).ready(function() {
    if($('form').is('#metadata-form')){ // check if we are in metadata form or not
        // apply chosen style to the fields
        $('#id_brain_region1').chosen({allow_single_deselect:true}); $('#id_brain_region2').chosen({allow_single_deselect:true}); $('#id_brain_region3').chosen({allow_single_deselect:true});
        $('#id_cell_type1').chosen({allow_single_deselect:true}); $('#id_cell_type2').chosen({allow_single_deselect:true}); $('#id_cell_type3').chosen({allow_single_deselect:true});
        $('#id_species,#id_strain,#id_gender,#id_development_stage').chosen({allow_single_deselect:true});
        $('#id_experimental_condition,#id_experimental_protocol').chosen({allow_single_deselect:true});
        $('#id_stain,id_slice_tickness').chosen({allow_single_deselect:true});
        $('#id_slicing_direction,#id_reconstruction_software').chosen({allow_single_deselect:true});
        $('#id_objective_type').chosen({allow_single_deselect:true});
        $('#id_numerical_units,#id_data_type').chosen({allow_single_deselect:true});
        var delayTimer;
        // keep track of selection order in brain region
        $('#id_brain_region3').on('change', function(){
            clearTimeout(delayTimer);
            delayTimer = setTimeout(function() {
                var selection = $('#id_brain_region3').getSelectionOrder();
                $('#region3order').val(selection);
                // console.log(selection);
            }, 100); // Will do the ajax stuff after XX ms
       });
        // keep track of selection order in cell type
        $('#id_cell_type3').on('change', function(){
            clearTimeout(delayTimer);
            delayTimer = setTimeout(function() {
                var selection = $('#id_cell_type3').getSelectionOrder();
                $('#celltype3order').val(selection);
                // console.log(selection);
            }, 100); // Will do the ajax stuff after XX ms
       });
    
       // set selection order to the pre saved version
        $(window).bind('load', function() {
            var reg_ord = document.getElementById('region3order').value;
            // console.log('->', $('#id_brain_region3').val() );
            if (reg_ord != '' && reg_ord != 'none' && reg_ord != 'None'){
                $('#id_brain_region3').setSelectionOrder($('#region3order').val().split(','), true);
            } else if ($('#id_brain_region3').val()) {
                // console.log('else');
                $('#region3order').val($('#id_brain_region3').getSelectionOrder());
            }
            var cell_ord = document.getElementById('celltype3order').value;
            if (cell_ord != 'none' && cell_ord != '' && cell_ord != 'None'){
                $('#id_cell_type3').setSelectionOrder($('#celltype3order').val().split(','), true);
            } else if ($('#id_cell_type3').val()) {
                $('#celltype3order').val($('#id_cell_type3').getSelectionOrder());
            }
        });
    }
});

// check new terms in the inserted values
$(document).ready(function(){
    var box_name = $(this).attr("name");
    $('input[name=new_species], input[name=new_strain], input[name=new_gender], input[name=new_development_stage], input[name=new_age_type], input[name=new_brain_region1], input[name=new_brain_region2], input[name=new_cell_type1], input[name=new_cell_type2], input[name=new_experimental_protocol], input[name=new_experimental_condition], input[name=new_stain], input[name=new_slicing_direction], input[name=new_reconstruction_software], input[name=new_objective_type], input[name=new_data_type]').keyup(function(){
    // $('input[name=new_strain], input[name=new_species]').keyup(function(){
        var box_name = $(this).attr("name");
        var select_id = 'id_' + box_name.replace("new_", "");
        // variable which is just typed in the form
        var typed = $('input[name='+box_name+']').val();
        var select_options = $.map($('#'+select_id+' option'), function(e) { return e.text; });
        termSet = FuzzySet(select_options);
        var msg = "";
        var hits = termSet.get(typed,null,0.5);
        // console.log(box_name);
        if(hits !=null ){
            for(var i = 0; i < hits.length; i++) {
                // index 1 is the score! thats why its not been take care of
                msg += "<a href=javascript:quickSelect('$select_id','$v')>"+hits[i][1]+"</a> ";
                msg = msg.replace('$v', select_options.indexOf(hits[i][1]));
                msg = msg.replace('$select_id', select_id);
            }
            $('#'+box_name+'_help').html("Similar hits: " + msg);
            // console.log(box_name);
        } else{
            $('#'+box_name+'_help').html("");
        }
    });
});

// selecting func
function quickSelect(select_id, opt) {
    // var sel = sel;
    var new_box = select_id.replace('id', 'new');
    // var new_box_value = $("input[name=" + new_box + "]").value();
    // var new_box_value = document.getElementsByName(new_box)[0].value;
    var help_id = new_box + "_help";
    var term_list = $.map($('#'+select_id+' option'), function(e) { return e.text; });
    document.getElementById(select_id).selectedIndex = parseInt(opt); 
    $("#"+select_id).trigger('chosen:updated');
    $("#"+help_id).html("");
    $("input[name=" + new_box + "]").val("");
    $("input[name=" + new_box + "]").hide(500);
}

function quickHits() {
    var new_fields = ["new_species", "new_strain", "new_gender", "new_development_stage", "new_age_type", "new_brain_region1", "new_brain_region2", "new_cell_type1", "new_cell_type2", "new_experimental_protocol", "new_experimental_condition", "new_stain", "new_slicing_direction", "new_reconstruction_software", "new_objective_type", "new_data_type"];
    new_fields.forEach(function(field){
        // console.log(field);
        var typed = $('input[name='+field+']').val();;
        var select_id = field.replace('new_', 'id_');
        var update_id = field + "_help";
        if (typed && typed.toLowerCase() !='none'){
            var term_list = $.map($('#'+select_id+' option'), function(e) { return e.text; });
            termSet = FuzzySet(term_list);
            var msg = "";
            var hits = termSet.get(typed,null,0.5);
            if(hits !=null ){
                for(var i = 0; i < hits.length; i++) {
                    msg += "<a href=javascript:quickSelect('$select_id','$v');>"+hits[i][1]+"</a> ";
                    msg = msg.replace('$v', term_list.indexOf(hits[i][1]));
                    // msg = msg.replace('$v', "this is\ a\ test");
                    msg = msg.replace('$select_id', select_id);
                    // console.log(msg);
                }
                // $("#"+update_id).html("Similar hits: " + msg);
                document.getElementById(update_id).innerHTML = "Similar hits: " + msg;
            }
        }
    });
} // end of quickHits()

// random lab generator in the form
$(function () {
    count = 0;
    wordsArray = [
        "<b>Egger Lab</b><br>Neurophysiology, Institute of Zoology<br>Universitat Regensburg, Germany",
        "<b>Rancillac Lab</b><br>Brain Plasticity Unit, ESPCI-ParisTech<br>PSL Research University, Paris, France",
        "<b>Olson Lab</b><br>Department of Neuroscience<br>Karolinska Institute, Stockholm, Sweden",
        "<b>Storm Lab</b><br>Department of Physiology<br>University of Oslo, Norway",
        "<b>Firestein Lab</b><br>Department of Cell Biology and Neuroscience<br>Rutgers University, Piscataway, New Jersey, USA",
        "<b>Lozano Lab</b><br>Toronto Western Research Institute<br>Krembil Discovery Tower, University Health Network, Ontario, Canada",
        "<b>Flores lab</b><br>Department of Psychiatry<br>Douglas Mental Health University Institute, Montreal, Quebec, Canada",
        "<b>Piccoli Lab</b><br>Department of Medical Biotechnology and Translational Medicine<br>Universit&aacute; degli Studi di Milano, Italy",
        "<b>Diamond Lab</b><br>Synaptic Physiology Section<br>NINDS, NIH, Bethesda, Maryland, USA",
        "<b>Nusser Lab</b><br>Institute of Experimental Medicine<br>Hungarian Academy of Sciences, Budapest, Hungary",
        "<b>Jaffe Lab</b><br>Division of Life Sciences<br>University of Texas at San Antonio, USA",
        "<b>Surmeier/Apkarian/Martina Lab</b><br>Department of Physiology<br>Northwestern University, Chicago, Illinois, USA",
        "<b>Munera Lab</b><br>Behavioral Neurophysiology Laboratory<br>Universidad Nacional, Sede Bogota, Colombia",
        "<b>Taylor Lab</b><br>Department of Ophthalmology, Casey Eye Institute<br>Oregon Health and Science University, Portland, USA",
        "<b>Leroy Lab</b><br>Center for neurophysique et physiologie<br>Descartes University, Paris, France",
        "<b>Sune's Lab</b><br>Department of Molecular Biology<br>Institute of Parasitology &amp; Biomedicine Lopez Neyra, Granada, Spain",
        "<b>Sweedler Lab</b><br>Department of Chemistry and Beckman Institute<br>University of Illinois, Urbana, USA",
        "<b>Kilb Lab</b><br>Institute of Physiology and Pathophysiology,<br>University Medical Center of the Johannes Gutenberg University, Mainz, Germany",
        "<b>Yang_SH Lab</b><br>Department of Life Sciences, Agricultural Biotechnology Center<br>National Chung Hsing University, Taichung, Taiwan",
        "<b>Mizrahi Lab</b><br>Department of Neurobiology, Alexander Silberman Institute of Life Sciences<br>Hebrew University, Jerusalem, Israel",
        "<b>Nacher Lab</b><br>Neurobiology Unit and Program in Basic and Applied Neurosciences<br>Cell Biology Department, Universitat de Val&egrave;ncia, Spain",
        "<b>Qiu Lab</b><br>Department of Basic Medical Sciences<br>University of Arizona College of Medicine, Phoenix, USA",
        "<b>Johnston_J Lab</b><br>School of Life Sciences<br>University of Sussex, Brighton, England, UK",
        "<b>Johnston Lab</b><br>Center for Learning and Memory<br>University of Texas at Austin, USA",
        "<b>Muotri Lab</b><br>Kavli Institute for Brain and Mind<br>University of California San Diego, La Jolla, USA",
        "<b>Moyer Lab</b><br>Departments of Psychology and Biological Sciences<br>University of Wisconsin, Milwaukee, USA",
        "<b>Frankland Lab</b><br>Hospital for Sick Children<br>Toronto, Ontario, Canada",
        "<b>Schaefer Lab (Fukunga Archive)</b><br>Division of Neurophysiology<br>MRC National Institute for Medical Research, London, England, UK",
        "<b>Schier Lab</b><br>Center for Brain Science &amp; Department of Molecular and Cellular Biology<br>Harvard University, Cambridge, Massachusetts, USA",
        "<b>Li-Jen Lee Lab</b><br>Graduate Institute of Anatomy and Cell Biology<br>National University, Taipei, Taiwan",
        "<b>Jefferis/Potter/Luo Lab</b><br>Department of Biological Sciences<br>University of Cambridge, England, UK",
        "<b>Rind Lab</b><br>Institute of Neuroscience<br>Newcastle University, England, UK",
        "<b>Sun/Prince Lab</b><br>Department of Neurology and Neurological Sciences,<br>Stanford University, California, USA",
        "<b>Pozzo-Miller Lab</b><br>Department of Neurobiology<br>The University of Alabama at Birmingham, Alabama, USA",
        "<b>Joels Lab</b><br>Department of Translational Neuroscience, Brain Center Rudolf Magnus,<br>University Medical Center Utrecht, Netherlands",
        "<b>Luebke (Joachim) Lab</b><br>Institute of Neuroscience and Medicine INM-2<br>Research Center Juelich, Germany",
        "<b>Erzurumlu Lab</b><br>Department of Anatomy and Neurobiology<br>University of Maryland School of Medicine, Baltimore, USA",
        "<b>Rubel Lab</b><br>Virginia Merrill Bloedel Hearing Research Center<br>Department of OtolaryngologyHead and Neck Surgery, Seattle, Washington, USA",
        "<b>Petersen Lab</b><br>Brain Mind Institute, Laboratory of Sensory Processing (lsens.epfl.ch)<br>Faculty of Life Sciences, EPFL, Lausanne, Switzerland",
        "<b>Palmer Lab</b><br>Florey Institute<br>University of Melbourne, Victoria, Australia",
        "<b>Strettoi Lab</b><br>CNR Neuroscience Institute<br>Pisa, Italy",
        "<b>Yamamoto Lab</b><br>Department of Neurology, College of Physicians and Surgeons<br>Columbia University, New York, United States",
        "<b>Nelson Lab (Ikeno archive)</b><br>Department of Neuroscience, Wexner Medical Center<br>The Ohio State University, Columbus, US",
        "<b>Kim Lab</b><br>Center for Functional Connectomics,<br>University of Debrecen, Hungary",
        "<b>De Schutter Lab</b><br>Computational Neuroscience Unit, Graduate University <br>Okinawa Institute of Science & Technology, Japan",
        "<b>Nolan Lab</b><br>Centre for Neuroscience Research and Centre for Integrative Physiology,<br>University of Edinburgh, Scotland, UK",
        "<b>Eroglu Lab</b><br>Department of Neurobiology<br>Duke University Medical Center, Durham, North Carolina, USA",
        "<b>Rhode Lab</b><br>Department of Physiology<br>University of Wisconsin, Madison, USA",
        "<b>Jan Lab</b><br>Departments of Physiology, Biochemistry and Biophysics<br>University of California at San Francisco, USA",
        "<b>Jinno Lab</b><br>Department of Anatomy and Neuroscience<br>Graduate School of Medical Sciences, Kyushu University, Japan",
        "<b>Johnson Lab</b><br>Department of Animal Sciences<br>University of Illinois at Urbana-Champaign, USA",
        "<b>Prince Lab</b><br>Department of Neurology and Neurological Sciences,<br>Stanford University, California, USA",
        "<b>Rudy Lab</b><br>New York University Neuroscience Institute<br>NYU School of Medicine, New York, USA",
        "<b>Perrone-Bizzozero Lab</b><br>Department of Neurosciences<br>University of New Mexico Health Sciences Center, Albuquerque, USA",
        "<b>Portera-Cailliau Lab</b><br>Department of Neurology and Neurobiology<br>University of California, Los Angeles, California, USA",
        "<b>Prida Lab</b><br>Instituto Cajal<br>Consejo Superior de Investigaciones Cientficas, Madrid, Spain",
        "<b>Franca Lab</b><br>Laboratory of Neurobiology II<br>Federal University of Rio de Janeiro, Brazil",
        "<b>Epsztein Lab</b><br>Institut National de la Sant&eacute; et Recherche M&eacute;dicale<br>Marseille, France",
        "<b>Luders Lab</b><br>Institute for Research in Biomedicine<br>The Barcelona Institute of Science and Technology, Spain",
        "<b>Linyi Chen Lab</b><br>Department of Medical Science<br>National Tsing Hua University, Hsinchu, Taiwan",
        "<b>Fisher Lab</b><br>UCL Institute of Neurology<br>London, England, UK",
        "<b>Levkowitz Lab</b><br>Department of Molecular Cell Biology<br>Weizmann Institute of Science, Rehovot, Israel",
        "<b>Rodrigues Lab</b><br>Life and Health Sciences Research Institute<br>School of Health Sciences, University of Minho, Braga, Portugal",
        "<b>Luikart Lab</b><br>Department of Physiology &amp; Neurobiology, Geisel School of Medicine<br>Dartmouth College, New Hampshire, USA",
        "<b>Petreanu Lab</b><br>Neuroscience Programme<br>Champalimaud Center for the Unknown, Lisbon, Portugal",
        "<b>Lee Lab</b><br>Laboratory of Gene Regulation and Development<br>NIH, Bethesda, Maryland, USA",
        "<b>Davis Lab</b><br>Department of Neuroscience<br>Scripps Research Institute, Jupiter, Florida, USA",
        "<b>Kaneko Lab</b><br>Department of Morphological Brain Science<br>Graduate School of Medicine, Kyoto University, Japan",
        "<b>Saghatelyan Lab</b><br>Cellular Neurobiology Unit<br>Institut Universitaire en sante mentale, Quebec City, Canada",
        "<b>Lewis Lab (Zaitsev Archive)</b><br>Department of Psychiatry<br>University of Pittsburgh, Pennsylvania, USA",
        "<b>Oliveira Lab</b><br>Interdisciplinary Centre for Neurosciences (IZN)<br>Heidelberg, Germany",
        "<b>Povysheva Lab</b><br>Department of Neuroscience<br>University of Pittsburgh, Pennsylvania, USA",
        "<b>Engert Lab</b><br>Center for Brain Science<br>Harvard University, Cambridge, Massachusetts, USA",
        "<b>Kawaguchi Lab</b><br>Division of Cerebral Circuitry,<br>National Institute for Physiological Sciences, Tokyo, Japan",
        "<b>Rieger Lab</b><br>Center for Regenerative Biology and Medicine<br>MDI Biological Laboratory, Salisbury Cove, Maine, USA",
        "<b>Denk Lab</b><br>Max Planck Institute for Medical Research<br>Heidelberg, Germany",
        "<b>Kabbani Lab</b><br>Krasnow Institute for Advanced Study, George Mason University<br>Fairfax, Virginia, USA",
        "<b>Ascoli Lab</b><br>Krasnow Institute for Advanced Study<br>George Mason University, Virginia, USA",
        "<b>Hansel Lab</b><br>Department of Neurobiology<br>University of Chicago, Illinois, USA",
        "<b>Rybak_Knaden Lab</b><br>Max Planck Institute for Chemical Ecology<br>Jena, Germany",
        "<b>Peng Lab</b><br>Allen Institute for Brain Science<br>Seattle, Washington, USA",
        "<b>Keyvani Lab</b><br>Institute of Neuropathology<br>University of Duisburg-Essen, Germany",
        "<b>Long Lab</b><br>Shanghai Medical College<br>Fudan University, Shanghai, China",
        "<b>Juraska Lab</b><br>Department of Psychology<br>University of Illinois at Urbana-Champaign, USA",
        "<b>Oguro-Ando Lab</b><br>University of Exeter Medical School<br>England, UK",
        "<b>Rumbaugh Lab</b><br>Department of Neuroscience<br>Scripps Research Institute, Jupiter, Florida, USA",
        "<b>Monyer Lab</b><br>Department Clinical Neurobiology, German Cancer Research Center,<br>Heidelberg University Medical Center, Germany",
        "<b>Lopez-Bendito Lab</b><br>Instituto de Neurociencias de Alicante<br>UMH-CSIC, Sant Joan d'Alacant, Spain",
        "<b>Arenkiel Lab</b><br>Department of Molecular &amp; Human Genetics<br>Baylor College of Medicine, Houston, Texas, USA",
        ];
    setInterval(function () {
      count++;
      $("#ack").fadeOut(400, function () {
        $(this).html(wordsArray[count % wordsArray.length]).fadeIn(400);
      });
    }, 2500);
  });